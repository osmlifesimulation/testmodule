﻿using System;
using NetTopologySuite.Geometries;
using OSMLSGlobalLibrary;
using OSMLSGlobalLibrary.Observable.Geometries;
using OSMLSGlobalLibrary.Observable.Geometries.Actor;
using OSMLSGlobalLibrary.Observable.Property;
using OSMLSGlobalLibrary.Map;
using OSMLSGlobalLibrary.Modules;

namespace TestModule
{
    public class TestModule : OSMLSModule
    {
        /// <summary>
        /// Скорость самолета.
        /// </summary>
        [ObservableProperty("Количество самолетов", true)]
        public int AirplanesCount
        {
            get => MapObjects.Get<Airplane>().Count;
            set
            {
                var count = AirplanesCount;
                while (count < value)
                {
                    MapObjects.Add(new Airplane(new Coordinate(
                        _Random.Next(0, 100000), 
                        _Random.Next(0, 100000)),
                        _Random.Next(0, 1000),
                        "Unknown")
                    );

                    count++;
                }

                while (count > value)
                {
                    MapObjects.Remove(MapObjects.Get<Airplane>()[0]);
                    count--;
                }
            }
        }

        // Тестовая точка.
        private PointActor _Point;

        // Тестовая линия.
        private LineStringActor _Line;

        // Тестовый полигон.
        private PolygonActor _Polygon;

        // Тестовый "самолет" 1.
        private Airplane _Airplane1;
        
        // Тестовый "самолет" 2.
        private Airplane _Airplane2;

        private Random _Random = new Random();

        protected override void Initialize()
        {
            #region создание базовых объектов
            // (Следует обратить внимание, что все координаты у всех геометрических объектов задаются в сферической проекции Меркатора (EPSG:3857).)
            // Создание координаты точки в начале координат.
            var pointCoordinate = new Coordinate(0, 0);
            // Создание стандартной точки в созданных ранее координатах.
            _Point = new PointActor(pointCoordinate);

            // Создание координат для линии.
            var lineCoordinates = new[] {
                _Point.Coordinate,
                new Coordinate(0, -2000000),
                new Coordinate(-3000000, -1500000)
            };
            // Создание стандартной кривой линии по ранее созданным координатам.
            _Line = new LineStringActor(lineCoordinates);

            // Создание координат полигона.
            var polygonCoordinates = new[] {
                    new Coordinate(4000000, 5000000),
                    new Coordinate(6000000, 0),
                    new Coordinate(6000000, 6000000),
                    new Coordinate(4000000, 5000000)
            };
            // Создание стандартного полигона по ранее созданным координатам.
            _Polygon = new PolygonActor(new ObservableLinearRing(polygonCoordinates));

            #endregion

            #region создание базовых объектов

            // Добавление созданных объектов в общий список, доступный всем модулям. Объекты из данного списка отображаются на карте.
            MapObjects.Add(_Point);
            MapObjects.Add(_Line);
            MapObjects.Add(_Polygon);

            #endregion

            #region создание кастомного объекта и добавление на карту, модификация полигона заменой точки

            // Координаты самолёта, сконвертированные из Lat/Lon координат. Примерно в аэропорту "Internacional de Carrasco".
            var airplaneCoordinate = MathExtensions.LatLonToSpherMerc(-34.831747, -56.020034);
            // Скорость самолета, опять же, в сферической проекции Меркатора.
            var airplaneSpeed = 1000;
            // Создание объекта класса "самолет", реализованного в рамках данного модуля.
            _Airplane1 = new Airplane(airplaneCoordinate, airplaneSpeed, "Boeing");
            // Добавление самолета в список объектов.
            MapObjects.Add(_Airplane1);
            
            _Airplane2 = new Airplane(MathExtensions.LatLonToSpherMerc(-32.831747, -54.020034), 0, "Airbus");
            MapObjects.Add(_Airplane2);

            // // Заменим одну из точек ранее созданного полигона только что созданным самолетом.
            // polygonCoordinates[2] = airplaneCoordinate;

            #endregion

            #region демонстрация получения данных из коллекции MapObjects
            // Коллекция MapObjects нужна не только для хранения и отображения объектов, но ещё и для удобного доступа к ним.

            // Попробуем получить все объекты, являющиеся строго точками.
            var onlyPoints = MapObjects.Get<PointActor>(); // Будет возвращена точка, созданная в самом начале.

            // А теперь получим все объекты, являющиеся точками и наследующиеся от точек (а также наследников наследников).
            var allPoints = MapObjects.GetAll<PointActor>(); // Будет возвращена точка, созданная в самом начале и самолет.

            // А теперь получим ВСЕ объекты на карте.
            var allMapObjects = MapObjects.GetAll<Geometry>(); // Будут возвращены все 4 созданных нами объекта.

            #endregion
        }

        /// <summary>
        /// Вызывается постоянно, здесь можно реализовывать логику перемещений и всего остального, требующего времени.
        /// </summary>
        /// <param name="elapsedMilliseconds">TimeNow.ElapsedMilliseconds</param>
        public override void Update(long elapsedMilliseconds)
        {
            // Двигаем самолет.
            MapObjects.Get<Airplane>().ForEach(airplane => airplane.MoveUpRight());

            if (_Random.NextDouble() < 0.01)
            {
                _Airplane1.Speed += 0.1;
            }
        }
    }

    #region объявления класса, унаследованного от точки, объекты которого будут иметь уникальный стиль отображения на карте

    /// <summary>
    /// Самолет, умеющий летать вверх-вправо с заданной скоростью.
    /// </summary>
    [CustomStyle(
        @"new style.Style({
            image: new style.Circle({
                opacity: 1.0,
                scale: 1.0,
                radius: 3,
                fill: new style.Fill({
                    color: 'rgba(255, 0, 255, 0.4)'
                }),
                stroke: new style.Stroke({
                    color: 'rgba(0, 0, 0, 0.4)',
                    width: 1
                }),
            })
        });
        ")] // Переопределим стиль всех объектов данного класса, сделав самолет фиолетовым, используя атрибут CustomStyle.
    internal class Airplane : PointActor // Унаследуем данный данный класс от стандартной точки.
    {
        /// <summary>
        /// Скорость самолета.
        /// </summary>
        [ObservableProperty("Скорость", true)]
        public double Speed { get; set; }
        
        /// <summary>
        /// Компания производитель.
        /// </summary>
        [ObservableProperty("Компания", false)]
        public string Company { get; }

        /// <summary>
        /// Примечание к рейсу.
        /// </summary>
        [ObservableProperty("Примечание", true)]
        public string Note { get; set; } = "";

        /// <summary>
        /// Конструктор для создания нового объекта.
        /// </summary>
        public Airplane(Coordinate coordinate, double speed, string company) : base(coordinate)
        {
            Speed = speed;
            Company = company;
        }

        /// <summary>
        /// Двигает самолет вверх-вправо.
        /// </summary>
        public void MoveUpRight()
        {
            X += Speed;
            Y += Speed;
        }
    }

    #endregion
}